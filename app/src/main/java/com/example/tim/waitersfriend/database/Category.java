package com.example.tim.waitersfriend.database;

public enum Category {
    BREAKFAST,
    STARTERS,
    MAINS,
    DESSERTS,
    CHILDREN
}
