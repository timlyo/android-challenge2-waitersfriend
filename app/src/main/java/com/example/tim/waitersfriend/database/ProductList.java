package com.example.tim.waitersfriend.database;


import java.util.Map;

public class ProductList {
    Map<String, Product> products;

    public ProductList(){
        //BREAKFAST
        this.addProduct(new Product("Pancakes", 300, Category.BREAKFAST));
        this.addProduct(new Product("Bagel", 300, Category.BREAKFAST));
        this.addProduct(new Product("Full English", 500, Category.BREAKFAST));

        //Starters
        this.addProduct(new Product("Nachos", 250, Category.STARTERS));
        this.addProduct(new Product("Soup", 250, Category.STARTERS));

        //main
        this.addProduct(new Product("Fish and Chips", 350, Category.MAINS));
        this.addProduct(new Product("Spaghetti Bolognese", 400, Category.MAINS));
    }
    
    private void addProduct(Product product){
        products.put(product.getName(), product);
    }

    public Product getProduct(String name){
        return products.get(name);
    }
}
