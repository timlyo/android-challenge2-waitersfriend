package com.example.tim.waitersfriend.database;


import java.util.ArrayList;
import java.util.List;

public class Product {
    private String name;
    private List<String> styles;
    private int cost; // in pence to resolve floating point errors
    private Category category;

    public Product(String name, int cost, Category category){
        this(name, cost, category, new ArrayList<String>());
    }

    public Product(String name, int cost, Category category, List<String> styles){

    }

    public String getName() {
        return name;
    }

    public List<String> getStyles() {
        return styles;
    }

    public int getCost() {
        return cost;
    }

    public Category getCategory(){
        return category;
    }
}
